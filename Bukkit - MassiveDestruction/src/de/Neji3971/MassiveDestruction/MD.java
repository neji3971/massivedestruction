package de.Neji3971.MassiveDestruction;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.BlockIterator;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

public class MD extends JavaPlugin implements CommandExecutor{

	File				configFile;
	FileConfiguration	config;

	@Override
	public void onEnable(){
		configFile = new File(getDataFolder(), "config.yml");
		try{
			firstRun();
		}catch(Exception y){
			y.printStackTrace();
		}
		config = new YamlConfiguration();
		loadYamls();
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		Player player;
		if(sender instanceof Player){
			player = (Player) sender;
		}else{
			sender.sendMessage("You must be a player to use this command.");
			return true;
		}
		if(cmd.getName().equalsIgnoreCase("destruction") && args.length != 4){
			if(args.length < 4) sender.sendMessage(ChatColor.RED + "Too few arguments!");
			else if(args.length > 4) sender.sendMessage(ChatColor.RED + "Too many arguments!");
			else sender.sendMessage(ChatColor.RED + "Wrong argument count!");
		}else if(cmd.getName().equalsIgnoreCase("destruction") && args.length == 4){
			sender.sendMessage(ChatColor.RED + "Let there be massive DESTRUCTION!");
			Location location = getTargetBlockLocation(player);
			if(location == null){
				sender.sendMessage(ChatColor.RED + "Invalid Location!");
				return true;
			}
			int width, length;
			double height, offset;
			try{
				width = Integer.parseInt(args[0]);
			}catch(NumberFormatException e){
				sender.sendMessage(ChatColor.RED + "Invalid input for carpet-width: " + args[0] + "!");
				return true;
			}
			try{
				length = Integer.parseInt(args[1]);
			}catch(NumberFormatException e){
				sender.sendMessage(ChatColor.RED + "Invalid input for carpet-length: " + args[1] + "!");
				return true;
			}
			try{
				height = Double.parseDouble(args[2]);
			}catch(NumberFormatException e){
				sender.sendMessage(ChatColor.RED + "Invalid input for spawn-height: " + args[2] + "!");
				return true;
			}
			try{
				offset = Double.parseDouble(args[3]);
			}catch(NumberFormatException e){
				sender.sendMessage(ChatColor.RED + "Invalid input for TNT-offset: " + args[3] + "!");
				return true;
			}
			location.add(((width * offset) / 2) * -1, height, ((length * offset) / 2) * -1);
			int count = 0;
			for(int i = 0; i < length; i++){
				sender.sendMessage("Progress: " + ((double) (i + 1) / (double) length) * 100 + "%");
				for(int i2 = 0; i2 < width; i2++){
					try{
						Thread.sleep(1);
					}catch(InterruptedException e){
						e.printStackTrace();
					}
					location.add(offset, 0.0, 0.0);
					player.getWorld().spawnEntity(location, EntityType.PRIMED_TNT);
					count++;
				}
				location.add(width * offset * -1, 0.0, offset);
			}
			player.sendMessage(ChatColor.RED + "" + count + " TNTs spawned!");
			return true;
		}
		return false;
	}

	private Location getTargetBlockLocation(Player player){
		Location location = null;
		for(Iterator<?> itr = new BlockIterator(player, 120); itr.hasNext();){
			Block block = (Block) itr.next();
			if(!block.getType().equals(Material.AIR)){
				location = block.getLocation();
				break;
			}
		}
		return location;
	}

	private void firstRun() throws Exception{
		if(!configFile.exists()){
			configFile.getParentFile().mkdirs();
			copy(getResource("config.yml"), configFile);
		}
	}

	private void copy(InputStream in, File file){
		try{
			OutputStream out = new FileOutputStream(file);
			byte[] buf = new byte[1024];
			int len;
			while((len = in.read(buf)) > 0){
				out.write(buf, 0, len);
			}
			out.close();
			in.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public void loadYamls(){
		try{
			config.load(configFile);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}